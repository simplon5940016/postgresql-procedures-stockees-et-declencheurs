# PostgreSQL - Procédures stockées et déclencheurs

## 1- Sauvegarde de la dernière lecture d'une piste

### Modifie la table track pour ajouter la colonne last_played :

```sql
ALTER TABLE track
ADD COLUMN last_played TIMESTAMP NULL DEFAULT NULL;

```

### Crée une procédure qui prendra l'identifiant d'une piste et qui renseignera le champ last_played à la date et heure actuelle

```sql
CREATE OR REPLACE PROCEDURE public.update_track_last_played(p_track_id integer)
LANGUAGE plpgsql AS $$
BEGIN
    UPDATE track
    SET last_played = CURRENT_TIMESTAMP
    WHERE track_id = p_track_id;
END;
$$;


call update_track_last_played(1);
```


## 2- Sauvegarde du nom précédent de l'artiste

### Modifie la table artist pour ajouter la colonne previous_name :

```sql
ALTER TABLE artist
ADD COLUMN previous_name VARCHAR(120) NULL DEFAULT NULL;
```


### Crée une procédure qui prendra en paramètre l'identifiant de l'artiste et un ancien nom, pour mettre à jour le champ previous_name avec la valeur équivalente

```sql
CREATE OR REPLACE PROCEDURE public.update_artist_previous_name(p_artist_id integer, p_previous_name VARCHAR(120))
LANGUAGE plpgsql
AS $$
BEGIN
    UPDATE artist
    SET previous_name = p_previous_name
    WHERE artist.artist_id = p_artist_id;
END;
$$;


CALL update_artist_previous_name(1, 'test');
```


## 3- Mise à jour de la date de création d'une facture

### Ajoute un champ created_at à la table invoice pour enregistrer la date de création.

```sql
ALTER TABLE invoice
ADD COLUMN created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
```


### Crée une trigger function pour mettre à jour la date de création d'une nouvelle facture.

```sql
CREATE OR REPLACE FUNCTION update_invoice_created_at()
RETURNS TRIGGER AS $$
BEGIN
    NEW.created_at := CURRENT_TIMESTAMP;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql ;
```

### Crée un trigger qui appelle la procédure après l'insertion d'une nouvelle facture.

```sql
CREATE TRIGGER set_invoice_created_at
AFTER UPDATE ON invoice
FOR EACH ROW
EXECUTE FUNCTION update_invoice_created_at();
```

### Exemple pour déclencher le trigger 

```sql
UPDATE invoice
SET created_at = CURRENT_DATE
WHERE invoice_id = 1;
```


## 4- Mise à jour de la date de modification d'une piste

### Ajoute un champ modified_at à la table track pour enregistrer la date de modification.

```sql
ALTER TABLE track
ADD COLUMN modified_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
```



### Crée une trigger function pour mettre à jour la date de modification d'une piste.

```sql
CREATE OR REPLACE FUNCTION update_track_modified_at()
RETURNS TRIGGER AS $$
BEGIN
    NEW.modified_at := CURRENT_TIMESTAMP;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql ;
```

### Crée un trigger qui appelle la procédure après la mise à jour d'une piste.


```sql
CREATE TRIGGER set_track_modified_at
AFTER UPDATE ON track
FOR EACH ROW
EXECUTE FUNCTION update_track_modified_at();
```


### Exemple pour déclencher le trigger 

```sql
UPDATE track
SET modified_at = CURRENT_DATE
WHERE track_id = 1;
```


## 5- Calcul automatique du montant total d'une facture


### Crée une trigger function pour calculer et mettre à jour le montant total d'une facture.


```sql
CREATE OR REPLACE FUNCTION update_invoice_total()
RETURNS TRIGGER AS $$
DECLARE
    updated_total numeric;
BEGIN
    SELECT SUM(quantity * unit_price)
    INTO updated_total
    FROM invoice_line
    WHERE invoice_id = NEW.invoice_id;

  
    UPDATE invoice
    SET total = updated_total
    WHERE invoice_id = NEW.invoice_id;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;


```


### Crée un trigger qui appelle la procédure après la mise à jour ou l'insertion d'une ligne de facture.


```sql
CREATE TRIGGER set_invoice_total
AFTER INSERT OR UPDATE ON invoice_line
FOR EACH ROW
EXECUTE FUNCTION update_invoice_total();
```


### Exemple pour déclencher le trigger 

#### Avec UPDATE 

```sql
UPDATE invoice_line
SET quantity = 3, unit_price = 15.00
WHERE invoice_line_id = 1;
```

#### Avec INSERT 

```sql

INSERT INTO invoice_line (invoice_id, track_id , quantity, unit_price)
VALUES (1, 1, 2, 10.00);

```

## 6- Supprime un album de manière sécurisée, en vérifiant d'abord qu'aucune piste ne lui est encore associée


### Crée une trigger function qui lève une exception si l'album supprimé est encore associé à au moins une piste.

```sql
CREATE OR REPLACE FUNCTION check_album_tracks()
RETURNS TRIGGER AS $$
BEGIN
    IF EXISTS (SELECT 1 FROM track WHERE album_id = OLD.album_id) THEN
        RAISE EXCEPTION 'Il y a encore un album associé à la piste, ID numéro %', OLD.album_id;
    END IF;

    RETURN OLD;
END;
$$ LANGUAGE plpgsql;
```



### Crée un trigger qui appelle la trigger function avant la suppression d'un album.

```sql
CREATE TRIGGER check_album_before_delete
BEFORE DELETE ON album
FOR EACH ROW
EXECUTE FUNCTION check_album_tracks();
```

### Test pour DELETE un album qui va déclencher le trigger 

```sql
DELETE FROM album WHERE album_id = 1;
```